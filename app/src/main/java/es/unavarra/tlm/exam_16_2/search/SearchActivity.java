package es.unavarra.tlm.exam_16_2.search;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import es.unavarra.tlm.exam_16_2.R;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        SearchListAdapter adapter = new SearchListAdapter();
        ListView list = (ListView) findViewById(R.id.search_friend_list);
        list.setAdapter(adapter);

        Button searchButton = (Button) findViewById(R.id.search_button);
        searchButton.setOnClickListener(new SearchListener(adapter));
    }
}
